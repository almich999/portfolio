using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Weapon
{
    public static Dictionary<AvailableWeapon, Weapon> availableWeapons = new Dictionary<AvailableWeapon, Weapon>() 
    {
        { AvailableWeapon.ak_47, new Weapon(AvailableWeapon.ak_47 ,"ak_47", "AK-47", 2700, 30, 90, ShootingMode.automatic, 600, Team.TT, 2.5f, 0.88f, 300, 36f, 143f, 111f, 0.31f, 0.775f) },
        { AvailableWeapon.m4a1_s, new Weapon(AvailableWeapon.m4a1_s ,"m4a1_s", "M4A1-S", 3100, 20, 40, ShootingMode.automatic, 600, Team.BOTH, 3.1f, 0.9f, 300, 33f, 132f, 92f, 0.27f, 0.2f) },
    };

    public AvailableWeapon Enum;
    public string name;
    public string friendlyName;
    public int price;
    public int fullMagazine;
    public int fullAmmo;
    public ShootingMode shootingMode;
    public int rof;
    public float shootingSpeed;
    public Team availableInteam;
    public float reloadTime;
    public float walkSpeed;
    public int rewardForKill;
    public float damage;
    public float damageHS_UA;
    public float damageHS_A;
    public float randomRecoil;
    public float armorPenetration;
    
    public Weapon(AvailableWeapon Enum, string name, string friendlyName, int price, int fullMagazine, int fullAmmo, ShootingMode shootingMode,
        int rof, Team availableInteam, float reloadTime, float walkSpeed, int rewardForKill, 
        float damage, float damageHS_UA, float damageHS_A, float randomRecoil, float armorPenetration)
    {
        this.Enum = Enum;
        this.name = name;
        this.friendlyName = friendlyName;
        this.price = price; //cofniete
        this.fullMagazine = fullMagazine; // cofniete
        this.fullAmmo = fullAmmo;
        this.shootingMode = shootingMode;
        this.rof = rof;
        this.shootingSpeed = 1.0f / (rof/60f);
        this.availableInteam = availableInteam;
        this.reloadTime = reloadTime;
        this.walkSpeed = walkSpeed;
        this.rewardForKill = rewardForKill;
        this.damage = damage;
        this.damageHS_UA = damageHS_UA;
        this.damageHS_A = damageHS_A;
        this.randomRecoil = randomRecoil;
        this.armorPenetration = armorPenetration;
}

    public enum ShootingMode
    {
        manual,
        semiautomatic,
        automatic
    }
}


public enum AvailableWeapon
{
    ak_47,
    m4a1_s
}