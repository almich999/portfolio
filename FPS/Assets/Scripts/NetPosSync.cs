using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NetPosSync : Photon.MonoBehaviour
{ 
    float multiplier = 6f;

    Vector3 targetPosition;
    Quaternion targetRotation;


    void Start()
    {
        
    }

    void Update()
    {
        if (photonView.isMine == false)
        {
            transform.position = Vector3.Lerp(transform.position, targetPosition, multiplier * Time.deltaTime);
            transform.rotation = Quaternion.Slerp(transform.rotation, targetRotation, multiplier * Time.deltaTime);
        }
    }

    void OnPhotonSerializeView(PhotonStream stream, PhotonMessageInfo info)
    {
        if (photonView.isMine)
        {
            // I am the player, sending my position to other players
            stream.SendNext(transform.position);
            stream.SendNext(transform.rotation);
        }
        else
        {
            // I am not the player, Receive position from other players
            targetPosition =  (Vector3)stream.ReceiveNext();
            targetRotation =  (Quaternion)stream.ReceiveNext();
        }
    }
}
