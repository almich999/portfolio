using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class GameManager : MonoBehaviour
{

    public static GameManager instance;

    void Start()
    {
        instance = this;   
    }

    void Update()
    {
        
    }

    public void SpawnPlayer()
    {
        Vector3 spawnPosition = Vector3.zero;
        Transform spawns = GameObject.Find("Map" + UnityEngine.SceneManagement.SceneManager.GetActiveScene().name + "/Spawns/" + Player.myPlayer.team.ToString()).transform;
        spawnPosition = spawns.GetChild(Random.Range(0, spawns.childCount)).position;
        GameObject myPlayerGO = PhotonNetwork.Instantiate("Player", spawnPosition, Quaternion.identity, 0);
        myPlayerGO.GetComponent<FirstPersonController>().enabled = true;
        myPlayerGO.GetComponent<Shooting>().enabled = true;
        myPlayerGO.GetComponent<PlayerGO>().enabled = true;
         myPlayerGO.transform.Find("CameraHolder").gameObject.SetActive(true);
        GetComponent<PhotonView>().RPC("SpawnPlayerRPC", PhotonTargets.AllBuffered, Player.myPlayer.nick, myPlayerGO.GetComponent<PhotonView>().viewID);
    }

    [PunRPC]
    void SpawnPlayerRPC(string nick, int pvID, PhotonMessageInfo pmi)
    {
        GameObject newPlayerGO = PhotonView.Find(pvID).gameObject;
        newPlayerGO.name = "Player_" + nick;
        Player player = Player.FindPlayer(pmi.sender);
        player.gameObject = newPlayerGO;
        newPlayerGO.GetComponent<PlayerGO>().myPlayer = player;

    }

    [PunRPC]
    void TakeHpServerRPC(PhotonPlayer victimPP, PhotonMessageInfo pmi)
    {
        float damage = 20f;
        Player victim = Player.FindPlayer(victimPP);
        victim.hp -= damage;
        victim.hp = Mathf.Clamp(victim.hp, 0f, 100f);

        if (victim.hp == 0f)
        {
            GetComponent<PhotonView>().RPC("KillVictimRPC", PhotonTargets.All, victimPP);
        }
        else
        {
            GetComponent<PhotonView>().RPC("TakeHpVictimRPC", PhotonTargets.Others, victimPP, damage);
        }

    }

    [PunRPC]
    void TakeHpVictimRPC(PhotonPlayer victimPP, float damage)
    {
        Player.FindPlayer(victimPP).hp -= damage;
        Debug.Log("TakeHpVictimRPC@GameManager: " + victimPP.NickName + " -" + damage + " HP");
    }

    [PunRPC]
    void KillVictimRPC(PhotonPlayer victimPP)
    {
        Player.FindPlayer(victimPP).gameObject.SetActive(false);
        Debug.Log(" KillVictimRPC@GameManager: " + victimPP.NickName + " killed");
    }

}
