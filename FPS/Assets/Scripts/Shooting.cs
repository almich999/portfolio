using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

public class Shooting : MonoBehaviour
{
    void Start()
    {
        
    }

    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            Shoot();
        }
    }

    public void Shoot()
    {
        Ray radius = new Ray(Camera.main.transform.position, Camera.main.transform.forward);

        RaycastHit[] strokes = Physics.RaycastAll(radius, 100f).OrderBy(e=>e.distance).ToArray();

        foreach(RaycastHit hit in strokes)
        {
            Debug.DrawLine(Camera.main.transform.position, hit.point, Color.blue, 1f);
            //Debug.Log("We hit on: " + hit.transform.name);

            if(hit.transform.root != transform)
            {
                if (hit.transform.GetComponent<MeshRenderer>())
                {
                    //hit.transform.GetComponent<MeshRenderer>().material.color = Color.red;
                }
                if (hit.transform.root.tag == "Player")
                {
                    Debug.Log("We hit a player");
                    //hit.transform.root.FindChild("Model").GetComponent<MeshRenderer>().material.color = Color.cyan;
                    GameManager.instance.GetComponent<PhotonView>().RPC("TakeHpServerRPC", PhotonTargets.MasterClient, hit.transform.root.GetComponent<PlayerGO>().myPlayer.pp);
                }
                break;

            }

 //           if (hit.transform)
 //           {
 //               if (hit.transform != transform)
 //               {
 //                   if (hit.transform.GetComponent<MeshRenderer>())
 //                   {
 //                       //hit.transform.GetComponent<MeshRenderer>().material.color = Color.red;
 //                       if (hit.transform.root.tag == "Player")
 //                       {
 //                           Debug.Log("We hit a player");
 //                           //hit.transform.root.FindChild("Model").GetComponent<MeshRenderer>().material.color = Color.cyan;
 //                           GameManager.instance.GetComponent<PhotonView>().RPC("TakeHpServerRPC", PhotonTargets.MasterClient, hit.transform.root.GetComponent<PlayerGO>().myPlayer.pp);
 //                       }
 //                           break;
 //                   }
 //                }
 //           }
            
        }


    }
}
