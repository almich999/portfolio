using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TestScript : MonoBehaviour
{
    public static bool initialized = false;

    void Start()
    {
        if (initialized == false)
        {
            SceneManager.LoadScene(0);
        }
    }

}
