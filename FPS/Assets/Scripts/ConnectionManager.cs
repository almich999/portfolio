using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

/// <summary>
/// Script for connecting to server and disconnecting
/// </summary>

public class ConnectionManager : Photon.MonoBehaviour
{
    GameManager gm;

    void Start()
    {
        gm = GetComponent<GameManager>();
    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.L))
            Player.DebugListPlayers();
    }

    public void Connect()
    {
        PhotonNetwork.sendRate = 20;
        PhotonNetwork.sendRateOnSerialize = 20;
        PhotonNetwork.ConnectUsingSettings("alpha");
    }

    void OnGUI()
    {
        GUI.Label(new Rect(0, 0, 200, 20), PhotonNetwork.connectionStateDetailed.ToString());
    }

    void OnJoinedLobby()
    {
        SceneManager.LoadScene(1);
    }

    void OnPhotonRandomJoinFailed()
    {
        PhotonNetwork.CreateRoom(null);
    }

    void OnLevelWasLoaded(int level)
    {
        if (level != 0)
            PhotonNetwork.JoinRandomRoom();
        GetComponent<GameManager>().enabled = true; 
    }

    void OnPhotonPlayerConnected(PhotonPlayer pp)
    {
        if (PhotonNetwork.isMasterClient)
        {
            int team = 0;
            if (Player.players[Player.players.Count - 1].team == Team.CT)
                team = 1;
            else
                team = 0;
            photonView.RPC("PlayerJoined", PhotonTargets.AllBuffered, pp, team); 
        }
    }

    void OnPhotonPlayerDisconnected(PhotonPlayer pp)
    {
        if (PhotonNetwork.isMasterClient)
        {
            photonView.RPC("PlayerLeft", PhotonTargets.AllBuffered, pp);
        }
    }

    [PunRPC]
    void PlayerJoined(PhotonPlayer pp, int team)
    {
        Player player = new Player();
        Player.players.Add(player);
        player.nick = pp.NickName;
        player.pp = pp;
        player.team = (Team)team;
        if (pp == PhotonNetwork.player)
        {
            Player.myPlayer = player;
            gm.SpawnPlayer();
        }
    }

    [PunRPC]
    void PlayerLeft(PhotonPlayer pp)
    {
        var tmpPlayer = Player.FindPlayer(pp);
        if (tmpPlayer != null)
        {
            Player.players.Remove(tmpPlayer);
        }
    }

    void OnCreatedRoom()
    {
        int team = Random.Range(0, 2);
        photonView.RPC("PlayerJoined", PhotonTargets.AllBuffered, PhotonNetwork.player, team);
    }
}
