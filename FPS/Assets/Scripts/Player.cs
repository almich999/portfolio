using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player
{
    public static Player myPlayer;
    public GameObject gameObject;
    public string nick = "";
    public PhotonPlayer pp;
    public float hp = 100f;
    public Team team;


    public static List<Player> players = new List<Player>();

    public static void DebugListPlayers()
    {
        string dbgS = "Debug the list of players: " + players.Count + ", All players:\n";
        int i = 0;
        foreach (var player in players)
        {
            dbgS += "ID" + i + ", nick: " + player.nick + ", team: " + player.team + "\n";
            i++;
        }
        Debug.Log(dbgS);
    }

    public static Player FindPlayer(PhotonPlayer pp)
    {
        for (int i = 0; i < players.Count; i++)
        {
            if (players[i].pp == pp)
                return players[i];
        }
        return null;
    }
}

public enum Team {CT, TT, BOTH}
