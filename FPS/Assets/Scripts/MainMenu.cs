using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class MainMenu : MonoBehaviour
{

    ConnectionManager cm;
    public InputField ifNick;

    void Start()
    {
        DontDestroyOnLoad(gameObject);
        cm = GetComponent<ConnectionManager>();
    }

    
    void Update()
    {
        
    }

    // Button voids //

    public void ButtonPlay()
    {
        if (ifNick.text.Length >= 3)
        {
            TestScript.initialized = true;
            PhotonNetwork.playerName = ifNick.text;
            cm.Connect();
        }
    }

}
