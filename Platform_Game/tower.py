import pygame, pgeng
from pygame.locals import *
import random
import numpy as np
import os

pygame.init()

pygame.mixer.init()

tower_music = pygame.mixer.Sound('sounds/tower.mp3')
jump_sound = pygame.mixer.Sound('sounds/jump.wav')
new_level = pygame.mixer.Sound('sounds/new_level.wav')
fail_sound = pygame.mixer.Sound('sounds/fail.wav')


def tower():
    # platform size
    pgeng.tile_size = 20

    width = 240
    height = 320

    screen = pgeng.Screen((height, width), vsync=0)
    display = screen.get_display()

    clock = pygame.time.Clock()

    # player spawn
    spawn = pygame.Vector2(990, -40)

    # player size
    entity = pgeng.Entity(spawn, (20, 25))

    # camera sets the player to the center of the screen, not the top left corner
    scroll_change = [(screen.size[i] + entity.rect.size[i]) * 0.473 for i in range(2)]
    scroll = entity.center - scroll_change

    jumping = False
    y_momentum = 0
    air_timer = 0
    tiles = []

    # pojedncze platformy
    def platform(x1, x2, r, y, s):
        for i in range(x1, x2, r):
            tiles.append(pgeng.Tile((i, y), s))

    # platformy symetryczne podwojne
    def platformx2(x1, x2, r, y, s):
        for i in range(x1, x2, r):
            tiles.append(pgeng.Tile((i, y), s))
            tiles.append(pgeng.Tile((i + abs(x1) + abs(x2), y), s))

    # begin platform
    platform(910, 1090, 20, 0, pgeng.tile_size)

    # walls
    for i in range(-3020, 0, 20):
        platform(910, 930, 20, i, pgeng.tile_size)
        platform(1070, 1090, 20, i, pgeng.tile_size)

    # floor on lvl 2
    platform(810, 910, 20, -3020, pgeng.tile_size)
    platform(1070, 1170, 20, -3020, pgeng.tile_size)

    # walls off the screen
    for i in range(-3120, -3020, 20):
        platform(810, 830, 20, i, pgeng.tile_size)
        platform(1170, 1190, 20, i, pgeng.tile_size)

    # lvl 1
    for i in range(-3000, -50, 60):
        x = random.randint(930, 1050)
        platform(x, x + 20, 20, i, pgeng.tile_size)

    # lvl 2
    for i in range(-6000, -3040, 60):
        x = random.randint(930, 1050)
        platform(x, x + 20, 20, i, pgeng.tile_size)

        # lvl 3
    for i in range(-9000, -6020, 60):
        x = random.randint(940, 1050)
        platform(x, x + 10, 10, i, 10)

    # lvl 4
    for i in range(-12000, -9020, 60):
        x = random.randint(950, 1050)
        platform(x, x + 5, 5, i, 5)

        # lvl 5
    for i in range(-15000, -12020, 60):
        x = random.randint(948, 1050)
        platform(x, x + 2, 2, i, 2)

        # extreme case
    #     platform(948,950,2,-120,2)
    #     platform(1050,1052,2,-60,2)

    # background music
    pygame.mixer.music.load('sounds/tower.mp3')
    pygame.mixer.music.play(-1)
    pygame.mixer.music.set_volume(0.3)

    # load high score
    if os.path.exists("data.txt"):
        with open("data.txt", "r") as file:
            high_score = int(file.read())
            file.close()
    else:
        high_score = 0

    maxscore = 0
    maxzone = 1
    play_fail_sound = True

    font2 = pygame.font.Font(None, 40)
    font1 = pygame.font.Font(None, 22)
    font = pygame.font.Font(None, 33)

    new_record = False

    while True:

        speed = 1.25
        jump = -4
        score = 0
        zone = 1

        # background
        display.fill((25, 25, 25))

        # time
        dt = pgeng.delta_time(clock, 144)

        # moving screen vertical (difficult)
        if entity.center.y < -60:
            scroll.y -= 0.5
        if entity.center.y < -3060:
            scroll.y -= 0.1
        if entity.center.y < -6060:
            scroll.y -= 0.1
        if entity.center.y < -9060:
            scroll.y -= 0.1
        if entity.center.y < -12060:
            scroll.y -= 0.1

        # move left and right
        keys = pygame.key.get_pressed()
        x_momentum = 0
        if keys[K_d]:
            x_momentum += speed * dt
        if keys[K_a]:
            x_momentum -= speed * dt

        # drawing platforms
        for tile in tiles:
            pygame.draw.rect(display, (255, 255, 255), Rect(tile.location - scroll, tile.size), 2, 3)

        # score
        score += int(abs(entity.center.y / 60))

        if score > maxscore:
            maxscore = score

        score1 = font1.render('SCORE', False, (255, 255, 255))
        display.blit(score1, (5, 5))

        score2 = font2.render('{}'.format(maxscore), False, (255, 255, 255))
        display.blit(score2, (20, 25))

        # zone
        zone = int(np.floor(-entity.center.y / 3030)) + 1

        if zone > maxzone:
            maxzone = zone

        score1 = font1.render('ZONE', False, (255, 255, 255))
        display.blit(score1, (265, 5))

        score2 = font2.render('{}'.format(maxzone), False, (255, 255, 255))
        display.blit(score2, (280, 25))

        # hisghscore
        score1 = font1.render('HIGH', False, (255, 255, 255))
        display.blit(score1, (13, 110))

        score2 = font1.render('SCORE', False, (255, 255, 255))
        display.blit(score2, (5, 130))

        score3 = font2.render('{}'.format(high_score), False, (255, 255, 255))
        display.blit(score3, (20, 150))

        # resetting the player when they fall off the map
        if entity.center.y > 200:
            entity.location = spawn

        # end game
        if scroll.y < entity.center.y - 300:

            if play_fail_sound:
                fail_sound.play()
                fail_sound.set_volume(0.1)
                play_fail_sound = False
            entity.location = spawn
            pygame.draw.line(display, (25, 25, 25), (92, 120), (228, 120), 80)
            textsurface1 = font.render('GAME OVER', False, (200, 0, 0))
            display.blit(textsurface1, (93, 80))

            # checking high score
            if maxscore > high_score:
                new_record = True
                high_score = maxscore
                with open("data.txt", "w") as file:
                    file.write(str(high_score))
                    file.close()

            if new_record == True:
                new_high_score = font1.render('NEW HIGH SCORE', False, (0, 200, 0))
                display.blit(new_high_score, (92, 110))
                score2 = font2.render('{}'.format(maxscore), False, (255, 255, 255))
                display.blit(score2, (150, 130))
            else:
                your_score = font1.render('SCORE:', False, (200, 0, 0))
                display.blit(your_score, (93, 120))
                score2 = font2.render('{}'.format(maxscore), False, (255, 255, 255))
                display.blit(score2, (170, 112))

        # collision detection
        y_momentum += 0.1 * dt
        collisions = entity.movement(pygame.Vector2(x_momentum, y_momentum * dt), tiles)
        if collisions['bottom']:
            y_momentum = 0
            air_timer = 0
            jumping = False
        else:
            air_timer += dt
        if collisions['top']:
            y_momentum = 0

        # drawing a player
        pygame.draw.rect(display, (168, 2, 168), Rect(entity.rect.topleft - scroll, entity.rect.size))

        for event in pygame.event.get():
            if event.type == QUIT:
                pgeng.quit_game()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pgeng.quit_game()

                # jump
                if event.key == K_w and air_timer < 15 and not jumping:
                    y_momentum = jump
                    jumping = True
                    jump_sound.play()
                    jump_sound.set_volume(0.1)

                # restart
                if event.key == K_r:
                    entity.location = spawn
                    scroll.y = entity.center.y - scroll_change[1]
                    maxscore = 0
                    maxzone = 1
                    play_fail_sound = True
                    new_record = False

        pygame.display.update()
        clock.tick(144)


if __name__ == "__main__":
    tower()