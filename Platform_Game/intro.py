import pygame, pgeng
from pygame.locals import *
import math
from sys import exit
import os
import matplotlib.pyplot as plt
import numpy as np
from random import randint

from pyvidplayer import Video
from main import *

pygame.init()

width = 1280  # szerokość okna
height = 960  # wysokość okna

resolution = (width, height)
display = pygame.display.set_mode(resolution, pygame.RESIZABLE)

vid = Video("intro.mp4")
vid.set_size((width, height))


def intro():
    run = True
    while run:
        vid.draw(display, (0, 0))
        vid.set_volume(0.2)
        pygame.display.update()

        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                vid.close()
                pygame.quit()
                exit()
            if event.type == pygame.KEYDOWN:
                vid.close()
                pygame.mixer.music.load('sounds/game.mp3')
                pygame.mixer.music.play(-1)
                pygame.mixer.music.set_volume(0.1)
                main()
            if event.type == pygame.MOUSEBUTTONDOWN:
                vid.close()
                pygame.mixer.music.load('sounds/game.mp3')
                pygame.mixer.music.play(-1)
                pygame.mixer.music.set_volume(0.1)
                main()


if __name__ == "__main__":
    intro()