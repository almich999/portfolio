import pygame
from pygame.locals import *

from pygame.locals import *
import math
from sys import exit
import os
import matplotlib.pyplot as plt
import numpy as np
from random import randint
from game import *

from main import *
from tower import *

pygame.init()

width = 1280  # szerokość okna
height = 960  # wysokość okna

resolution = (width, height)
display = pygame.display.set_mode(resolution, pygame.RESIZABLE)
bg_img = pygame.image.load('draw/background.png')

pygame.mixer.init()
music = pygame.mixer.Sound('sounds/game.mp3')

# tower_music = pygame.mixer.Sound('sounds/tower.mp3')
click_sound = pygame.mixer.Sound('sounds/click.wav')

pygame.mixer.music.load('sounds/game.mp3')
pygame.mixer.music.play(-1)
pygame.mixer.music.set_volume(0.1)


class Image:
    def __init__(self, x, y, w, h, name):
        self.x_cord = x
        self.y_cord = y
        self.image = pygame.image.load(name)
        self.width = w
        self.height = h

    def tick(self):
        pass

    def draw(self):
        self.image1 = pygame.transform.scale(self.image, (self.width, self.height))
        display.blit(self.image1, (self.x_cord, self.y_cord))


class Button:

    def __init__(self, x, y, file_name):
        self.x_cord = resolution[0] / x
        self.y_cord = resolution[1] / y
        self.button_image = pygame.image.load(f"buttons/{file_name}_button.png")
        self.hovered_button_image = pygame.image.load(f"buttons/{file_name}_button_hovered.png")
        self.width = self.button_image.get_width()
        self.height = self.button_image.get_height()
        self.image1 = pygame.transform.scale(self.button_image, (self.width, self.height))
        self.image2 = pygame.transform.scale(self.hovered_button_image, (self.width, self.height))
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)  # tworzymy hitbox przycisku
        self.clicked = True

    def tick(self):
        # będziemy sprawdzać czy na  hitboxie przycisku znajduje się kursor
        if pygame.mouse.get_pressed()[0] == 1 and not self.hitbox.collidepoint(pygame.mouse.get_pos()):
            self.clicked = True
        if pygame.mouse.get_pressed()[0] == 0:
            self.clicked = False

        if self.hitbox.collidepoint(pygame.mouse.get_pos()) and self.clicked == False:
            if pygame.mouse.get_pressed()[0] == 1:
                return True

    def draw(self):
        if self.hitbox.collidepoint(
                pygame.mouse.get_pos()):  # jeżeli na hitboxie przycisku znajduje się kursor to przycisk sie podswietli (grafika sie podmieni)
            display.blit(self.image1, (self.x_cord, self.y_cord))
        else:
            display.blit(self.image2, (self.x_cord, self.y_cord))


def levels():
    run = True
    display.fill((25, 25, 25))
    x = 0
    p = resolution[1] / 100

    bg_image = Image(resolution[0] - resolution[0], resolution[1] - resolution[1], resolution[0], resolution[1],
                     "draw/background.png")

    one_b = Button(x=20, y=1.9, file_name="one")
    two_b = Button(x=8.3, y=1.9, file_name="two")
    three_b = Button(x=5.2, y=1.9, file_name="three")
    four_b = Button(x=3.4, y=4.1, file_name="four")
    five_b = Button(x=1.54, y=4.1, file_name="five")

    six_b = Button(x=1.325, y=1.9, file_name="six")
    seven_b = Button(x=1.215, y=1.9, file_name="seven")
    eight_b = Button(x=1.12, y=1.9, file_name="eight")

    back_b = Button(x=100, y=29, file_name="back")

    while run:

        x += 0.1

        pygame.time.Clock().tick(60)
        for event in pygame.event.get():

            if event.type == QUIT:
                pygame.quit()

        if one_b.tick():
            click_sound.play()
            click_sound.set_volume(0.1)
            game(spawn=pygame.Vector2(-10, 940))

        if two_b.tick():
            click_sound.play()
            click_sound.set_volume(0.1)
            game(spawn=pygame.Vector2(-10, 260))

        if three_b.tick():
            click_sound.play()
            click_sound.set_volume(0.1)
            game(spawn=pygame.Vector2(-10, -440))

        if four_b.tick():
            click_sound.play()
            click_sound.set_volume(0.1)
            game(spawn=pygame.Vector2(-10, -1440))

        if five_b.tick():
            click_sound.play()
            click_sound.set_volume(0.1)
            game(spawn=pygame.Vector2(-10, -2440))

        if back_b.tick():
            click_sound.play()
            click_sound.set_volume(0.1)
            main()

        bg_image.tick()
        bg_image.draw()

        one_b.tick()
        one_b.y_cord = resolution[1] / 1.9 + p * np.sin(x * resolution[1] / 600)
        # one_b.hitbox = pygame.Rect(one_b.x_cord,one_b.y_cord, one_b.width, one_b.height)
        one_b.draw()

        two_b.tick()
        two_b.y_cord = resolution[1] / 1.9 + p * np.sin(x * resolution[1] / 600)
        # two_b.hitbox = pygame.Rect(two_b.x_cord,two_b.y_cord, two_b.width, two_b.height)
        two_b.draw()

        three_b.tick()
        three_b.y_cord = resolution[1] / 1.9 + p * np.sin(x * resolution[1] / 600)
        # three_b.hitbox = pygame.Rect(three_b.x_cord,three_b.y_cord, three_b.width, three_b.height)
        three_b.draw()

        four_b.tick()
        four_b.y_cord = resolution[1] / 4.1 + p * np.sin(x * resolution[1] / 600)
        # four_b.hitbox = pygame.Rect(four_b.x_cord,four_b.y_cord, four_b.width, four_b.height)
        four_b.draw()

        five_b.tick()
        five_b.y_cord = resolution[1] / 4.1 + p * np.sin(x * resolution[1] / 600)
        # five_b.hitbox = pygame.Rect(five_b.x_cord,five_b.y_cord, five_b.width, five_b.height)
        five_b.draw()

        six_b.tick()
        six_b.y_cord = resolution[1] / 1.9 + p * np.sin(x * resolution[1] / 600)
        # six_b.hitbox = pygame.Rect(six_b.x_cord,six_b.y_cord, six_b.width, six_b.height)
        six_b.draw()

        seven_b.tick()
        seven_b.y_cord = resolution[1] / 1.9 + p * np.sin(x * resolution[1] / 600)
        # seven_b.hitbox = pygame.Rect(seven_b.x_cord,seven_b.y_cord, seven_b.width, seven_b.height)
        seven_b.draw()

        eight_b.tick()
        eight_b.y_cord = resolution[1] / 1.9 + p * np.sin(x * resolution[1] / 600)
        # eight_b.hitbox = pygame.Rect(eight_b.x_cord,eight_b.y_cord, eight_b.width, eight_b.height)
        eight_b.draw()

        back_b.tick()
        # back_b.y_cord = resolution[1]/29 + p*np.sin(x*resolution[1]/600)
        back_b.draw()

        pygame.display.update()


if __name__ == "__main__":
    levels()