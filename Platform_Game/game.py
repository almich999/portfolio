import pygame, pgeng
from pygame.locals import *
import random

pygame.init()

pygame.mixer.init()


def game(spawn=pygame.Vector2(-10, 940)):
    teleport_sound = pygame.mixer.Sound('sounds/teleport.mp3')
    jump_sound = pygame.mixer.Sound('sounds/jump.wav')
    new_level = pygame.mixer.Sound('sounds/new_level.wav')

    # single platform
    def platform(x1, x2, r, y, s):
        for i in range(x1, x2, r):
            tiles.append(pgeng.Tile((i, y), s))


    # single platform
    def lift_platform(x1, x2, r, y, s):
        for i in range(x1, x2, r):
            lift_tiles.append(pgeng.Tile((i, y), s))

    # teleport platform
    def teleport_platform(x1, x2, r, y, s):
        for i in range(x1, x2, r):
            teleport_tiles.append(pgeng.Tile((i, y), s))

    # przezroczyste platformy
    def transparent_platform(x1, x2, r, y, s):
        for i in range(x1, x2, r):
            transparent_tiles.append(pgeng.Tile((i, y), s))

    # pojedncze platformy RED
    def red_platform(x1, x2, r, y, s):
        for i in range(x1, x2, r):
            red_tiles.append(pgeng.Tile((i, y), s))

    # platformy ostatnie
    def end_platform(x1, x2, r, y, s):
        for i in range(x1, x2, r):
            end_tiles.append(pgeng.Tile((i, y), s))

    # platformy symetryczne podwojne
    def platformx2(x1, x2, r, y, s):
        for i in range(x1, x2, r):
            tiles.append(pgeng.Tile((i, y), s))
            tiles.append(pgeng.Tile((i + abs(x1) + abs(x2), y), s))

    # napisy
    def text(y1, y2, x1, x2, f, text, x, y):
        if entity.center.y < y1 and entity.center.y > y2 and entity.center.x > x1 and entity.center.x < x2:
            font = pygame.font.Font(None, f)
            textsurface1 = font.render(text, False, (255, 255, 255))
            display.blit(textsurface1, (x, y))
            # fps

    def update_fps():
        fps = str(int(clock.get_fps()))
        fps_text = font.render(fps, 1, pygame.Color("cyan"))
        return fps_text

    # platforma wymiary x na x
    pgeng.tile_size = 20
    screen = pgeng.Screen((320, 240), vsync=0)
    display = screen.get_display()

    clock = pygame.time.Clock()
    delta = 0.0
    # miejsce spawnu gracza
    # spawn = pygame.Vector2(-10, 940)

    # spawn = pygame.Vector2(90,-1770)
    # rozmiar gracza
    entity = pgeng.Entity(spawn, (20, 25))
    # this makes it so the camera sets the player to the center of the screen, not the top left corner
    scroll_change = [(screen.size[i] + entity.rect.size[i]) * 0.473 for i in range(2)]
    scroll = entity.center - scroll_change

    jumping = False
    y_momentum = 0
    air_timer = 0

    lift_tiles = []
    tiles = []
    red_tiles = []
    transparent_tiles = []
    teleport_tiles = []
    end_tiles = []
    # piewsza wspolrzedna x od lewej, 2 wspolrzedna do konca x , 3 wspolrzedna odstep platformy, (i,y) y wysokosc

    # jak podchodzisz to sie pojawia

# platform(-70, -50, 20, -2556, pgeng.tile_size)
# platform(10, 30, 20, -2556, pgeng.tile_size)
# platform(-30, -10, 20, -2556, pgeng.tile_size)
# platform(50, 70, 20, -2556, pgeng.tile_size)
#
# platform(-50, -30, 20, -2478, pgeng.tile_size)
# platform(30, 50, 20, -2478, pgeng.tile_size)
# platform(-10, 10, 20, -2478, pgeng.tile_size)
#
# platform(-30, 30, 20, -2400, pgeng.tile_size)

    # platforma początkowa
    platform(-20, 20, 20, -2400, 20)

    # podest na górze
    platform(-40, 40, 20, -3720, 20)

    # drabinka

    for d in range(42):
        platform(20, 40, 20, -3740 - d * 20, 20)

    # podest końcowy

    platform(20, 200, 20, -4560, 20)

    end_platform(200, 260, 20, -4560, 20)

    ##########################################  LVL ##########################################

    ### ściany ###

    for k in range(1, 100):
        platform(180, 200, 20, -2400 - k * 20, 20)
        platform(-200, -180, 20, -2400 - k * 20, 20)

    ### windy ###

    # ilość wind #
    number_of_lifts = 21

    lift_move = []
    leftm = []
    rightm = []
    lmove = []
    lift_size = []
    blocks_in_lift = []

    for i in range(number_of_lifts):
        lift_move.append(random.randint(-40, 40))
        leftm.append(-180)

        lmove.append(random.choice([-1, 1]))
        lift_size.append(random.choice([10, 12, 15, 20]))
        if lift_size[i] == 20:
            blocks_in_lift.append(random.choice([2, 3]))
        elif lift_size[i] == 15:
            blocks_in_lift.append(random.choice([3, 4]))
        else:
            blocks_in_lift.append(random.choice([2, 3, 4, 5, 6]))
        rightm.append(180 - lift_size[i]*blocks_in_lift[i])

    total_lifts_blocks = 0

    for l in range(len(blocks_in_lift)):
        total_lifts_blocks += blocks_in_lift[l]

    for n in range(number_of_lifts):
        lift_platform(int(lift_move[n]), int(lift_move[n] + blocks_in_lift[n] * lift_size[n]), lift_size[n],-2460 - n * 60, lift_size[n])




    ########################################## 5 LVL ##########################################

    platform(-30, 30, 20, -1920, pgeng.tile_size)
    end_platform(-30, 30, 20, -1920, pgeng.tile_size)

    platform(-210, -190, 20, -1840, pgeng.tile_size)
    platform(-200, -180, 20, -1780, pgeng.tile_size)
    platform(-175, -155, 20, -1720, pgeng.tile_size)
    platform(-170, -160, 10, -1660, 10)
    platform(-270, -260, 10, -1600, 10)
    platform(-180, -170, 10, -1560, 10)

    platform(-180, -170, 10, -1520, 10)

    for i in range(-1520, -1480, 20):
        tiles.append(pgeng.Tile((150, i), pgeng.tile_size))
    platform(150, 190, 20, -1540, pgeng.tile_size)
    platform(210, 230, 20, -1460, pgeng.tile_size)
    platform(290, 310, 20, -1400, pgeng.tile_size)

    red_platform(210, 250, 20, -1580, pgeng.tile_size)
    platform(210, 250, 20, -1580, pgeng.tile_size)

    platform(250, 270, 20, -1600, pgeng.tile_size)

    for i in range(-1880, -1400, 20):
        tiles.append(pgeng.Tile((310, i), pgeng.tile_size))

    platform(230, 310, 20, -1720, pgeng.tile_size)

    red_platform(110, 190, 20, -1740, pgeng.tile_size)
    platform(110, 190, 20, -1740, pgeng.tile_size)

    red_platform(110, 210, 20, -1620, pgeng.tile_size)
    platform(110, 210, 20, -1620, pgeng.tile_size)

    for i in range(-1860, -1760, 20):
        tiles.append(pgeng.Tile((-110, i), pgeng.tile_size))

    # pionowy teleport
    for i in range(-1440, -1400, 20):
        tiles.append(pgeng.Tile((110, i), pgeng.tile_size))
        tiles.append(pgeng.Tile((-130, i), pgeng.tile_size))
        teleport_tiles.append(pgeng.Tile((110, i), pgeng.tile_size))
        teleport_tiles.append(pgeng.Tile((-130, i), pgeng.tile_size))

    # poziomy teleport
    for i in range(-170, -110, 20):
        tiles.append(pgeng.Tile((i, -1400), pgeng.tile_size))
        tiles.append(pgeng.Tile((i + 280, -1400), pgeng.tile_size))
        teleport_tiles.append(pgeng.Tile((i, -1400), pgeng.tile_size))
        teleport_tiles.append(pgeng.Tile((i + 280, -1400), pgeng.tile_size))

        tiles.append(pgeng.Tile((i, -1460), pgeng.tile_size))
        tiles.append(pgeng.Tile((i + 280, -1460), pgeng.tile_size))
        teleport_tiles.append(pgeng.Tile((i, -1460), pgeng.tile_size))
        teleport_tiles.append(pgeng.Tile((i + 280, -1460), pgeng.tile_size))

    platform(50, 90, 20, -1760, pgeng.tile_size)
    platform(-70, -50, 20, -1760, pgeng.tile_size)
    platform(-90, -70, 20, -1700, pgeng.tile_size)
    platform(70, 90, 20, -1700, pgeng.tile_size)
    platform(-10, 30, 20, -1640, pgeng.tile_size)
    platform(-90, -50, 20, -1640, pgeng.tile_size)
    platform(-50, -10, 20, -1580, pgeng.tile_size)
    platform(-30, -10, 20, -1500, pgeng.tile_size)
    platform(70, 90, 20, -1520, pgeng.tile_size)
    platform(70, 90, 20, -1460, pgeng.tile_size)
    platform(-30, 30, 20, -1400, pgeng.tile_size)
    ########################################## 4 LVL ##########################################
    for i in range(-1760, -400, 20):
        tiles.append(pgeng.Tile((90, i), pgeng.tile_size))
        tiles.append(pgeng.Tile((-110, i), pgeng.tile_size))
    end_platform(-50, 50, 20, -1100, pgeng.tile_size)
    platform(-50, 50, 20, -1100, pgeng.tile_size)
    platform(-40, 40, 20, -1030, pgeng.tile_size)
    platform(-30, 30, 20, -960, pgeng.tile_size)
    platform(-20, 20, 20, -890, pgeng.tile_size)
    platform(-15, 15, 15, -820, 15)
    platform(-10, 10, 20, -750, pgeng.tile_size)
    platform(-5, 5, 10, -680, 10)
    platform(-10, 10, 20, -610, pgeng.tile_size)
    platform(-20, 20, 20, -540, pgeng.tile_size)
    platform(-30, 30, 20, -470, pgeng.tile_size)
    platform(-40, 40, 20, -400, pgeng.tile_size)
    ########################################## 3 LVL PIRAMIDA ##########################################
    platform(-10, 10, 20, -70, pgeng.tile_size)
    end_platform(-10, 10, 20, -70, pgeng.tile_size)
    ########################################## 2 schody  ##########################################
    platform(-80, -40, 20, 0, pgeng.tile_size)
    transparent_platform(0, 20, 20, 25, pgeng.tile_size)
    platform(60, 100, 20, 50, pgeng.tile_size)
    transparent_platform(140, 160, 20, 75, pgeng.tile_size)
    platform(200, 240, 20, 100, pgeng.tile_size)
    # transparent_platform(280,300,20,125,pgeng.tile_size)
    ########################################## 1 schody  ##########################################
    platform(340, 380, 20, 150, pgeng.tile_size)
    transparent_platform(280, 320, 20, 175, pgeng.tile_size)
    platform(220, 260, 20, 200, pgeng.tile_size)
    # transparent_platform(160,200,20,225,pgeng.tile_size)
    platform(100, 140, 20, 250, pgeng.tile_size)
    transparent_platform(40, 80, 20, 275, pgeng.tile_size)
    ########################################## parter ##########################################
    platform(-20, 20, 20, 300, pgeng.tile_size)
    ########################################## 2 LVL ##########################################
    ########################################## 6 pietro ##########################################
    # 1 nad dwoma pionowymi
    platform(-20, 20, 20, 640, pgeng.tile_size)
    end_platform(-20, 20, 20, 640, pgeng.tile_size)
    ########################################## 5 pietro ##########################################
    # gora 2 dalej wyzej
    platformx2(-140, -100, 20, 700, pgeng.tile_size)
    ########################################## 4 pietro ##########################################
    # gora 2  dalej
    red_platform(100, 140, 20, 760, pgeng.tile_size)
    platformx2(-140, -100, 20, 760, pgeng.tile_size)
    ########################################## 3 pietro ##########################################
    # 2 wyzej obok
    platformx2(-210, -170, 20, 820, pgeng.tile_size)
    # 1 wyzej
    platform(-10, 10, 20, 820, pgeng.tile_size)
    ########################################## 2 pietro ##########################################
    # gora 2 ale obok
    platformx2(-200, -180, 20, 880, pgeng.tile_size)
    # gora 2
    platformx2(-60, -40, 20, 880, pgeng.tile_size)
    ########################################## 1 pietro ##########################################
    # lewo prawo
    platformx2(-140, -60, 20, 940, pgeng.tile_size)
    ########################################## parter ##########################################
    # lewo i prawo
    platformx2(-220, -160, 20, 1000, pgeng.tile_size)
    # poczatek
    platform(-60, 60, 20, 1000, pgeng.tile_size)

    Rgb = 168
    rGb = 2
    rgB = 168
    while True:
        ###########################################################################################################################
        # usuwanie starych pozycji #

        for k in range(total_lifts_blocks):
            lift_tiles.pop(-1)

        # generowanie nowych pozycji #

        for n in range(number_of_lifts):

            if lift_move[n] < leftm[n] or lift_move[n] > rightm[n]:
                lmove[n] *= -1

            lift_move[n] += lmove[n]
            lift_platform(int(lift_move[n]), int(lift_move[n] + blocks_in_lift[n] * lift_size[n]), lift_size[n],-2460 - n * 60, lift_size[n])
        ###########################################################################################################################
        speed = 1.25
        jump = -4
        # tło
        display.fill((25, 25, 25))
        # czas
        dt = pgeng.delta_time(clock,144)
        delta += pgeng.delta_time(clock, 144) / 100.0
        # print(delta)
        # ruch ekranu w poziomie
        scroll.x += (entity.center.x - scroll.x - scroll_change[0]) / 20 * dt
        # ruch ekranu w pionie
        scroll.y += (entity.center.y - scroll.y - scroll_change[1]) / 20 * dt

        # ruch lewo prawo
        keys = pygame.key.get_pressed()
        x_momentum = 0
        if keys[K_d]:
            x_momentum += speed * dt
        if keys[K_a]:
            x_momentum -= speed * dt
        # rysowanie platform
        for tile in tiles:
            pygame.draw.rect(display, (255, 255, 255), Rect(tile.location - scroll, tile.size), 2, 3)

        for tile in transparent_tiles:
            pygame.draw.rect(display, (100, 100, 100), Rect(tile.location - scroll, tile.size), 2, 3)

        for tile in red_tiles:
            pygame.draw.rect(display, (255, 0, 0), Rect(tile.location - scroll, tile.size), 2, 3)

        for tile in teleport_tiles:
            pygame.draw.rect(display, (0, 255, 255), Rect(tile.location - scroll, tile.size), 2, 3)

        for tile in end_tiles:
            pygame.draw.rect(display, (0, 255, 0), Rect(tile.location - scroll, tile.size), 2, 3)

        for tile in lift_tiles:
            pygame.draw.rect(display, (255, 255, 0), Rect(tile.location - scroll, tile.size), 2, 3)

            # resetowanie jak wejdzie na czerwony
        if entity.center.y < 750 and entity.center.y > 745 and entity.center.x > 89 and entity.center.x < 149:
            entity.location = spawn
        # lvl4 wieza  gora
        if entity.center.y < -1735 and entity.center.y > -1762 and entity.center.x > 109 and entity.center.x < 190:
            entity.location = pygame.Vector2(-10, -1440)

            # lvl4 wieza  srodek
        if entity.center.y < -1615 and entity.center.y > -1642 and entity.center.x > 109 and entity.center.x < 221:
            entity.location = pygame.Vector2(-10, -1440)

            # lvl4 wieza  srodek male
        if entity.center.y < -1575 and entity.center.y > -1602 and entity.center.x > 209 and entity.center.x < 251:
            entity.location = pygame.Vector2(-10, -1440)

            # lvl4 wieza  teleport prawy
        if entity.center.y < -1390 and entity.center.y > -1500 and entity.center.x > 130 and entity.center.x < 150:
            teleport_sound.play()
            teleport_sound.set_volume(0.1)
            entity.location = pygame.Vector2(-160, -1405)

            # resetowanie postaci jak spadnie na 1 lvl
        if entity.center.y > 1200:
            entity.location = spawn
        # na 2 lvl
        if entity.center.y < 580 and entity.center.y > 560:
            new_level.play()
            new_level.set_volume(0.2)
        if entity.center.y < 580 and entity.center.y > 500:
            entity.location = pygame.Vector2(-10, 260)
            # na lvl3
        if entity.center.y < -130 and entity.center.y > -160:
            new_level.play()
            new_level.set_volume(0.2)
        if entity.center.y < -130 and entity.center.y > -280:
            entity.location = pygame.Vector2(-10, -440)

        # na lvl4
        if entity.center.y < -1150 and entity.center.y > -1170:
            new_level.play()
            new_level.set_volume(0.2)
        if entity.center.y < -1150 and entity.center.y > -1300:
            entity.location = pygame.Vector2(-10, -1440)

        # na lvl5
        if entity.center.y < -1980 and entity.center.y > -2000:
            new_level.play()
            new_level.set_volume(0.2)
        if entity.center.y < -1980 and entity.center.y > -2350:
            entity.location = pygame.Vector2(-10, -2415)

        # pgeng.TextButton.render('LA', (20,1000), 'small')  #########################

        # napisy
        # text(-1400,-1420,-10,10,40,'LEVEL 4',105,200)
        text(-2400, -2420, -10, 10, 40, 'LEVEL 5', 105, 200)
        text(-1400, -1420, -10, 10, 40, 'LEVEL 4', 105, 200)
        # text(-1100,-1130,-30,30,40,'JUMP',120,170)
        text(-380, -420, -10, 10, 40, 'LEVEL 3', 105, 200)
        # text(-80,-100,-10,10,40,'JUMP',120,200)
        text(300, 280, -10, 10, 40, 'LEVEL 2', 105, 200)
        text(640, 620, -10, 10, 40, 'JUMP', 120, 200)
        text(1000, 980, -10, 10, 40, 'LEVEL 1', 105, 200)
        # fps
        font = pygame.font.Font(None, 25)
        display.blit(update_fps(), (0, 0))
        # detekcja kolizji
        y_momentum += 0.1 * dt
        collisions = entity.movement(pygame.Vector2(x_momentum, y_momentum * dt), tiles + lift_tiles)
        if collisions['bottom']:
            y_momentum = 0
            air_timer = 0
            jumping = False
        else:
            air_timer += dt
        if collisions['top']:
            y_momentum = 0
        # rysowanie gracza
        pygame.draw.rect(display, (Rgb, rGb, rgB), Rect(entity.rect.topleft - scroll, entity.rect.size))

        for event in pygame.event.get():
            if event.type == QUIT:
                pgeng.quit_game()
            if event.type == KEYDOWN:
                if event.key == K_ESCAPE:
                    pgeng.quit_game()

                # skakanie
                if event.key == K_w and air_timer < 15 and not jumping:
                    jump_sound.play()
                    jump_sound.set_volume(0.1)
                    y_momentum = jump
                    jumping = True
                ###########################################################################################################################
                elif event.key == K_w and entity.center.x == 10 and -3700 >= entity.center.y >= -4560:
                    jump_sound.play()
                    jump_sound.set_volume(0.1)
                    y_momentum = jump
                    jumping = True

        pygame.display.update()
        clock.tick(144)


if __name__ == "__main__":
    game()