import pygame
from pygame.locals import *
import numpy as np
from random import randint

# from classes import *
pygame.init()
pygame.mixer.init()
width = 1280
height = 720
resolution = (width, height)
window = pygame.display.set_mode(resolution)
bg_img = pygame.image.load('space/galaxy.png')


class CButton:
    def __init__(self, x_cord, y_cord, file_name):
        self.x_cord = x_cord
        self.y_cord = y_cord
        self.button_image = pygame.image.load('main menu/controls_button.PNG')
        self.hovered_button_image = pygame.image.load('main menu/controls_button_hovered.PNG')
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.button_image.get_width(),
                                  self.button_image.get_height())  # tworzymy hitbox przycisku

    def tick(self):
        if self.hitbox.collidepoint(
                pygame.mouse.get_pos()):  # będziemy sprawdzać czy na  hitboxie przycisku znajduje się kursor
            if pygame.mouse.get_pressed()[0]:
                return True

    def draw(self, window):
        if self.hitbox.collidepoint(
                pygame.mouse.get_pos()):  # jeżeli na hitboxie przycisku znajduje się kursor to przycisk sie podswietli (grafika sie podmieni)
            window.blit(self.hovered_button_image, (self.x_cord, self.y_cord))
        else:
            window.blit(self.button_image, (self.x_cord, self.y_cord))


class Button:
    def __init__(self, x_cord, y_cord, file_name):
        self.x_cord = x_cord
        self.y_cord = y_cord
        self.button_image = pygame.image.load('main menu/play_button.PNG')
        self.hovered_button_image = pygame.image.load('main menu/play_button_hovered.PNG')
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.button_image.get_width(),
                                  self.button_image.get_height())  # tworzymy hitbox przycisku

    def tick(self):
        if self.hitbox.collidepoint(
                pygame.mouse.get_pos()):  # będziemy sprawdzać czy na  hitboxie przycisku znajduje się kursor
            if pygame.mouse.get_pressed()[0]:
                return True

    def draw(self, window):
        if self.hitbox.collidepoint(
                pygame.mouse.get_pos()):  # jeżeli na hitboxie przycisku znajduje się kursor to przycisk sie podswietli (grafika sie podmieni)
            window.blit(self.hovered_button_image, (self.x_cord, self.y_cord))
        else:
            window.blit(self.button_image, (self.x_cord, self.y_cord))


class Shipdown:
    def __init__(self):
        self.x_cord = 1380
        self.y_cord = randint(0,
                              550)  # statek będzie pojawiał się na współrzędnej x = 1380 i y która będzie losowa z przedziału (0,550)
        self.image = pygame.image.load('space/minishipenemydown.png')

    def tick(self):
        pass

    def draw(self):
        window.blit(self.image, (self.x_cord, self.y_cord))


class Shipup:
    def __init__(self):
        self.x_cord = -300
        self.y_cord = randint(0,550)  # statek będzie pojawiał się na współrzędnej x = -300 i y która będzie losowa z przedziału (0,550)
        self.image = pygame.image.load('space/minishipenemyup.png')

    def tick(self):
        pass

    def draw(self):
        window.blit(self.image, (self.x_cord, self.y_cord))


class Meteor:
    def __init__(self):
        self.x_cord = 1280
        self.y_cord = randint(0, 600)  # meteor będzie pojawiał sie na losowej wysokości z prawej strony ekranu
        self.image = pygame.image.load('space/meteor.PNG')
        self.width = randint(140,200)  # meteory będą miały różne szerokości i wysokości zachowując pierwotne proporcje obrazka
        self.height = self.width - 50
        self.image1 = pygame.transform.scale(self.image, (self.width, self.height))
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)

    def tick(self):
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)

    def draw(self, s):
        self.x_cord -= 25 + s
        window.blit(self.image1, (self.x_cord, self.y_cord))


class Player:
    def __init__(self):
        self.x_cord = 50
        self.y_cord = 370
        self.image = pygame.image.load('space/minishipdark.png')
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.speed = 30
        self.hitbox = pygame.Rect(self.x_cord + 10, self.y_cord + 10, self.width - 20, self.height - 20)
        self.x_screen = resolution[0] / 4

    def tick(self, keys):

        if keys[pygame.K_w]:
            self.image = pygame.image.load('space/minishipdark.png')
            if self.y_cord >= -10:
                self.y_cord -= self.speed
        else:
            self.image = pygame.image.load('space/minishipdark.png')

        if keys[pygame.K_s]:
            if self.y_cord <= height - self.image.get_height():
                self.y_cord += self.speed

        if keys[pygame.K_a]:
            if self.x_cord >= 0:
                self.x_cord -= self.speed
            self.image = pygame.image.load('space/miniship3dark.png')

        if keys[pygame.K_d]:
            if self.x_cord <= resolution[0] / 4:
                self.x_cord += self.speed
            self.image = pygame.image.load('space/miniship1dark.png')

        if keys[pygame.K_a] & keys[pygame.K_d]:
            self.image = pygame.image.load('space/minishipdark.png')

        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)

    def draw(self):

        if self.x_cord >= resolution[0] / 4:  # blokujemy możliwości latania statkiem po całym ekranie do 1/4 szerokości ekranu
            self.x_screen = resolution[0] / 4
        else:
            self.x_screen = self.x_cord
        window.blit(self.image, (self.x_screen, self.y_cord))


class Laser:
    def __init__(self, player):
        self.x_cord = player.x_cord + player.image.get_width() / 2.2  # laser bedzie rysował sie w miejscu gdzie statek ma działko
        self.y_cord = player.y_cord + player.image.get_height() / 1.65
        self.image = pygame.image.load('space/laser.PNG')
        self.width = self.image.get_width()
        self.height = self.image.get_height()
        self.hitbox = pygame.Rect(self.x_cord, self.y_cord, self.width, self.height)
        self.speed = 50

    def tick(self):
        self.hitbox = pygame.Rect(self.x_cord + self.width, self.y_cord + self.height, self.width, self.height)

    def draw(self):
        self.x_cord += self.speed
        window.blit(self.image, (self.x_cord, self.y_cord))

    def __del__(self):
        pass


def pause():  # definiujemy pauze która zatrzyma grę w momencie porażki
    paused = True
    while paused:
        for event in pygame.event.get():
            if event.type == pygame.QUIT:
                pygame.quit()
                quit()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_r:
                    paused = False
                    level()

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    paused = False
                    main()


pygame.time.Clock()
# tworzymy 100 losowych wartości x i 100 wartości losowych y aby generować 100 gwiazdek losowo rozmieszczonych na ekranie
randx1 = np.random.randint(1280, size=30)
randy1 = np.random.randint(720, size=30)
randx2 = np.random.randint(1280, size=30)
randy2 = np.random.randint(720, size=30)
randx3 = np.random.randint(1280, size=40)
randy3 = np.random.randint(720, size=40)

miniboom = pygame.image.load('space/miniboom.png')
boom = pygame.image.load('space/boom.png')

boom_sound = pygame.mixer.Sound('sound/boom.wav')
laser_sound = pygame.mixer.Sound('sound/laser.mp3')


###################################################################################

def level():
    pygame.mixer.music.load('sound/level1.mp3')
    pygame.mixer.music.play(1)  # puszczamy w tle poziomu muzzykę, -1 oznacza że piosenka będzie się zapętlać po zakonczeniu
    pygame.mixer.music.set_volume(0.3)

    k = 0
    k >= 0 and k <= 20
    s = 2
    j = 0
    k2 = 0
    speed = 0
    run = True
    player = Player()
    clock = 0
    shipsdown = []
    shipsup = []
    meteors = []
    lasers = []

    while run:
        window.fill((0, 0, 0))
        window.blit(bg_img, (j, 0))  # rysujemy tło wypełniające okno
        window.blit(bg_img, (width + j, 0))  # rysujemy drugie tło przyklejone do poprzedniego z prawej strony
        if (j == -width):  # jeżeli pierwsze tło wyjedzie całe poza ekran, to rysujemy tło przyklejone do drugiego tła które aktualnie pokrywa całe okno
            window.blit(bg_img, (width + j, 0))
            j = 0
        j -= 1

        clock += pygame.time.Clock().tick(60)
        for event in pygame.event.get():

            # Definiujemy rolę przycisków. co powinno się dziac po ich nacisnieciu

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_KP6:
                    if k < 20:
                        k += s
                        k2 += k
                        press = True
                    else:
                        press = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_KP4:
                    if k > 0:
                        k -= s
                        k2 -= k
                        press = True
                    else:
                        press = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_KP5:
                    k = 0
                    k2 = 0
                    press = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_q:
                    run = False
                    press = True
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:  # po nacisnieciu esc przeniesie nas do menu głównego
                    main()
                    press = True

            if event.type == QUIT:
                run = False

            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_SPACE:
                    laser_sound.play()
                    laser_sound.set_volume(0.05)
                    lasers.append(Laser(player))

        # będziemy tworzyć trzy warstwy gwiazdek (każda warstwa będzie miała inny rozmiar gwiazdek) , gwiazdki reprezentują małe kółka które poruszają się w lewo.
        # Kiedy gwiazdki znikną z lewej strony ekranu, to pojawiają się po prawej stronie
        # Gwiazdki dodatkowo mają nałożony efekt migotania poprzez zmiany koloru na czarny
        for i in range(40):
            if (randx3[i] + randy3[i] * i) % 240 <= 200:
                col1 = 255
            else:
                col1 = 0
            if randx3[i] < -10:
                randx3[i] = 1290
                randy3[i] = np.random.randint(2, 718)
            randx3[i] -= 0.5 + k
            pygame.draw.circle(window, (col1, col1, col1), (randx3[i], randy3[i]), 1)

        for i in range(30):
            if (randx2[i] + randy2[i] * i) % 240 <= 200:
                col2 = 255
            else:
                col2 = 0
            if randx2[i] < -10:
                randx2[i] = 1290
                randy2[i] = np.random.randint(2, 718)
            randx2[i] -= 1.5 + k
            pygame.draw.circle(window, (col2, col2, col2), (randx2[i], randy2[i]), 3)

        for i in range(30):
            if (randx1[i] + randy1[i] * i) % 240 <= 200:
                col3 = 255
            else:
                col3 = 0
            if randx1[i] < -10:
                randx1[i] = 1290
                randy1[i] = np.random.randint(2, 718)
            randx1[i] -= 2.5 + k
            pygame.draw.circle(window, (col3, col3, col3), (randx1[i], randy1[i]), 5)

        keys = pygame.key.get_pressed()

        # jeżeli modulo 200 części całkowitej z clocka będzie wynosił 0 to z prawdobodobieństwem 1/2 będzie sie pojawiał statek bliżej obserwatora albo dalej
        if int(clock) % 200 == 0:
            if np.random.random() < 0.5:
                shipsdown.append(Shipdown())
            else:
                shipsup.append(Shipup())

        if int(clock) % 20 == 0:  # meteoryty się pojawiają jeżeli modulo 20 z czesci całkowitej z clock będzie równy 0
            meteors.append(Meteor())

        for shipdown in shipsdown:
            shipdown.tick()
            # int(k/s) jest naszą prędkościa wyświetlaną na ekranie w zakresie 0-10
            if int(k / s) > 5:
                shipdown.x_cord -= -20 + k * 2
            if int(k / s) == 5:
                shipdown.x_cord = shipdown.x_cord
            if int(k / s) < 5:
                shipdown.x_cord += 20 - k * 2

            shipdown.draw()

        for meteor in meteors:
            meteor.tick()
            meteor.draw(k)

        player.tick(keys)

        pygame.display.update()

        player.draw()

        for laser in lasers:
            laser.tick()
            laser.draw()

        for shipup in shipsup:
            shipup.tick()

            if int(k / s) < 5:
                shipup.x_cord += 20 - k * 2
            if int(k / s) == 5:
                shipup.x_cord = shipup.x_cord
            if int(k / s) > 5:
                shipup.x_cord -= -20 + k * 2

            shipup.draw()

            # tworzymy zmienną k2, która jest odpowiedzialna za dodawanie punktów ze względu na aktualną prędkość
        # im większa prędkość tym więcej punktów zdobywa gracz
        if k2 > 0:
            speed += abs((k2 ** 2) / 30000)

        myfont = pygame.font.SysFont('Arial', 50)
        textsurface1 = myfont.render('Speed: {}'.format(int(k / s)), False, (255, 255, 255))
        window.blit(textsurface1, (10, 0))

        myfont = pygame.font.SysFont('Arial', 50)
        textsurface1 = myfont.render('Score: {}'.format(int(clock / 100 + speed)), False, (255, 255, 255))
        window.blit(textsurface1, (900, 0))

        for meteor in meteors:
            if meteor.x_cord <= -200:
                meteors.remove(meteor)  # jeżeli współrzędna meteora przekroczy x = -200 to zostaje on usunięty

        for laser in lasers:
            if laser.x_cord >= width + 100:
                lasers.remove(laser)  # jeżeli współrzędna lasera przekroczy x = 1380 to zostaje on usunięty

        # jeżeli statki oddalą się na wspólrzędne x - -10000 lub 10000 to zostają one usunięte
        for shipup in shipsup:
            if shipup.x_cord <= -10000 or shipup.x_cord >= 10000:
                shipsup.remove(shipup)

        for shipdown in shipsdown:
            if shipdown.x_cord <= -10000 or shipdown.x_cord >= 10000:
                shipsdown.remove(shipdown)

        for meteor in meteors:
            for laser in lasers:
                if meteor.hitbox.colliderect(
                        laser.hitbox):  # jeżeli hitbox meteora pokryje sie z hitboxm lasera to pojawia sie wybuch, dzwiek i laser wraz z meteorem znikają
                    meteors.remove(meteor)
                    lasers.remove(laser)
                    window.blit(miniboom, (meteor.x_cord + meteor.width / 6, meteor.y_cord + meteor.height / 7))
                    boom_sound.play()
                    boom_sound.set_volume(0.1)

            if player.hitbox.colliderect(
                    meteor.hitbox):  # jeżeli hitbox gracza spotka sie z hitboxem meteoru to statek wybucha a gra sie konczy i wyswietlają sie napisy
                window.blit(boom, (player.x_screen, player.y_cord))
                pygame.mixer.music.stop()
                boom_sound.play()
                boom_sound.set_volume(0.4)
                myfont = pygame.font.SysFont('Arial', 100)
                textsurface1 = myfont.render('GAME OVER', False, (255, 255, 255))
                myfont = pygame.font.SysFont('Arial', 60)
                textsurface2 = myfont.render('Your score: {}'.format(int(clock / 100 + speed)), False, (255, 255, 255))
                myfont = pygame.font.SysFont('Arial', 40)
                textsurface3 = myfont.render('press ESC to go to the main menu or R to restart game', False,
                                             (255, 255, 255))
                window.blit(textsurface1, (330, 200))
                window.blit(textsurface2, (450, 350))
                window.blit(textsurface3, (160, 450))
                pygame.display.update()
                pause()

        pygame.display.update()

    pygame.quit()


###############################################################################
# tworzymy okno z controlsami. tło się porusza
def controls():
    controls = pygame.image.load('main menu/controls.PNG')
    run = True
    clock = 0
    j = 0

    while run:
        window.fill((0, 0, 0))
        window.blit(bg_img, (j, 0))
        window.blit(bg_img, (width + j, 0))
        if j == -width:
            window.blit(bg_img, (width + j, 0))
            j = 0
        j -= 1

        pygame.time.Clock().tick()
        for event in pygame.event.get():
            if event.type == pygame.KEYDOWN:
                if event.key == pygame.K_ESCAPE:
                    run = False
                    press = True

            if event.type == QUIT:
                run = False

        window.blit(controls, (460, 0))
        pygame.display.update()


######################################################################################
# tworzymy menu główne, które oprócz przycisków posiada muzykę oraz zdjęcie w tle
def main():
    run = True
    background = pygame.image.load('main menu/menu_background.PNG')
    play_button = Button(560, 530, 'main menu/play_button.PNG')
    controls_button = CButton(560, 620, 'main menu/control_button.PNG')

    pygame.mixer.music.load('sound/menu.mp3')
    pygame.mixer.music.play(-1)
    pygame.mixer.music.set_volume(0.4)

    while run:

        pygame.time.Clock().tick(60)
        for event in pygame.event.get():

            if event.type == QUIT:
                pygame.quit()

        if play_button.tick():
            pygame.mixer.music.stop()
            level()

        if controls_button.tick():
            controls()

        window.blit(background, (0, 0))
        play_button.draw(window)
        controls_button.draw(window)
        pygame.display.update()


if __name__ == '__main__':
    main()
