using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class pipemover : MonoBehaviour
{
    [SerializeField]
    private float _speed = 5;

    private void Update()
    {
        transform.position += Vector3.left * _speed * Time.deltaTime;
    }
}
